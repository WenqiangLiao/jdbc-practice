package db_connect;

import model.Student;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class StudentRepo {
    private DbConnector dbConnector;
    
    public StudentRepo() {
        dbConnector = new DbConnector();
    }
    
    
    public boolean addStudent(Student student) throws SQLException {
        // Need to be implemented
        String sql = String.format("insert into student (name, age, gender, phone) values ('%s', %d, '%s', '%s')", student.getName(), student.getAge(), student.getGender(), student.getPhone());
        try (
            Connection con = dbConnector.createConnect();
            Statement st = con.createStatement()) {
            return st.executeUpdate(sql) > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
    
    public List<Student> findStudents() {
        // Need to be implemented
        String sqlOne = String.format("insert into student (name, age, gender, phone) values ('%s', %d, '%s', '%s')", "LiSi", 18, "nv", "15567879876");

        List<Student> students = new ArrayList<>();
        String sqlTwo = "select * from student";
        try (
                Connection con = dbConnector.createConnect();
                Statement st = con.createStatement()
                ){
            st.executeUpdate(sqlOne);
            ResultSet resultSet = st.executeQuery(sqlTwo);
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String gender = resultSet.getString("name");
                int age = resultSet.getInt("age");
                String sex = resultSet.getString("gender");
                String phone = resultSet.getString("phone");

                Student aStudent = new Student(id, gender, age, sex, phone);
                students.add(aStudent);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return students;
    }
    
    public boolean updateStudent(Student student) {
        // Need to be implemented
        String sql = String.format("update student set name='%s', age='%d', gender='%s', phone='%s'",
                student.getName(), student.getAge(), student.getGender(), student.getPhone());
        try (
                Connection con = dbConnector.createConnect();
                Statement st = con.createStatement()
                ) {
            return st.executeUpdate(sql) > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
    
    public boolean deleteStudent(Student student) {
        // Need to be implemented
        String sql = String.format("delete from student where name='%s'", student.getName());

        try (Connection con = dbConnector.createConnect();
              Statement st = con.createStatement()
        ) {
            return st.executeUpdate(sql) > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}
