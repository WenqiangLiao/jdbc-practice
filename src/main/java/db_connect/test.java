package db_connect;

import model.Student;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class test {
    public static void main(String[] args) throws SQLException {
        StudentRepo students = new StudentRepo();
        List<Student> studentList = students.findStudents();

        Student student = studentList.stream().filter(s -> s.getName().equals("LiSi")).findFirst().get();
        System.out.println(student.getName());
        System.out.println(student.getAge());
        System.out.println(student.getGender());
        System.out.println(student.getPhone());
    }
}
